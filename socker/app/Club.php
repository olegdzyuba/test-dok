<?php

declare(strict_types=1);

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Query\Builder;

class Club extends Model
{
    const PROBABILITY = 5; // вероятность в процентном отношении забить гол в минуту

    /**
     * @return BelongsTo
     */
    public function getAllPlayers()
    {
        return Player::where('club_id', $this->id)->get();
    }
}

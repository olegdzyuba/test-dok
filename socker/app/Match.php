<?php

declare(strict_types=1);

namespace App;

use Exception;
use Illuminate\Database\Eloquent\Model;

class Match extends Model
{
    const COUNT_CLUBS = 2;
    const COUNT_HALF  = 2;
    const TIME_HALF   = 45;

    protected $fillable = [
        'count_clubs',
        'time_half',
        'count_half',
        'created_at',
        'updated_at',
        'match_result'
    ];
}

<?php

declare(strict_types=1);

namespace App;

use Illuminate\Database\Eloquent\Model;

class Player extends Model
{
    public static function findRandomForward(int $clubId): int
    {
        return self::where(['club_id' => $clubId, 'role' => 'not_goalkeeper'])->inRandomOrder()->first()->id;
    }
}

<?php

function parser(string $data): string
{
	$response  = '';
	$dataArray = json_decode($data, true);

	foreach ($dataArray as $item) {
		$propertyData = [];
		$innerData    = explode('/', $item);

		$response .= '<div class="product">';

		foreach ($innerData as $value) {
			if(strripos($value, ':') !== false) {
				$afterInner = explode(':', $value);
				$propertyData[$afterInner[0]][] = $afterInner[1];
			} else {
				$propertyData[$afterInner[0] ?? 'underfined'][] = $value;
			}
		}

		foreach ($propertyData as $type => $property) {
			$response .= '<span class="product-character">';
			$response .= $type.': '.(implode('/', $property));
			$response .= '</span>';
		}

		$response .= '</div>';
	}

	return $response;

}

$data = '["Классификация API:SN\/Классификация ACEA:C3\/Тип:Синтетическое","Классификация API:SM\/Тип:Синтетическое","Классификация API:SN\/Классификация API:CF\/Классификация ACEA:A3\/B3\/Тип:Полусинтетическое","Классификация API:SN\/Классификация API:CF\/Классификация ACEA:A3\/B4\/Тип:Синтетическое","Классификация ACEA:C4\/Тип:Синтетическое","Классификация API:SN\/Классификация ACEA:C2\/Тип:Синтетическое","Классификация API:SN\/Тип:Синтетическое","Классификация API:SN\/Классификация ACEA:A3\/B4\/Тип:Синтетическое","Классификация ACEA:C2\/Тип:Синтетическое","Классификация API:SN\/Классификация ACEA:C3\/Тип:Синтетическое","Классификация ACEA:B3\/Тип:Полусинтетическое","Классификация ACEA:C3\/Тип:Синтетическое","Тип:Полусинтетическое","Классификация ACEA:C3\/Тип:Синтетическое","Классификация ACEA:C3\/Тип:Синтетическое","Классификация API:CF\/Тип:Синтетическое","Тип:Полусинтетическое","Классификация API:SM\/Классификация API:CF\/Классификация ACEA:A3\/B4\/Классификация ACEA:C3\/Тип:Синтетическое","Классификация API:SJ\/Классификация API:CD\/Тип:Синтетическое","Классификация API:SN\/Классификация API:CF\/Классификация ACEA:C3\/Тип:Синтетическое"]';

echo parser($data);